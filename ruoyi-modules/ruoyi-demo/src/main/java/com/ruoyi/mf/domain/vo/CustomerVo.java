package com.ruoyi.mf.domain.vo;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.mf.domain.Customer;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.mybatisflex.annotation.RelationOneToMany;
import com.ruoyi.mf.domain.Goods;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 客户主表视图对象 mf_customer
 *
 * @author 数据小王子
 * @date 2023-12-06
 */
@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Customer.class)
public class CustomerVo extends BaseEntity implements Serializable
{

    @Serial
    private static final long serialVersionUID = 1L;

     /** 客户id */
    @ExcelProperty(value = "客户id")
    private Long customerId;

     /** 客户姓名 */
    @ExcelProperty(value = "客户姓名")
    private String customerName;

     /** 手机号码 */
    @ExcelProperty(value = "手机号码")
    private String phonenumber;

     /** 客户性别 */
    @ExcelProperty(value = "客户性别", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_user_gender")
    private String gender;

     /** 客户生日 */
    @ExcelProperty(value = "客户生日")
    private Date birthday;

     /** 客户描述 */
    @ExcelProperty(value = "客户描述")
    private String remark;


    /** 商品子表信息 */
    @RelationOneToMany(selfField = "customerId", targetField = "customerId")
    private List<Goods> goodsList;

}
