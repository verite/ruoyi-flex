package com.ruoyi.mf.service;

import java.util.List;
import com.ruoyi.mf.domain.Customer;
import com.ruoyi.mf.domain.vo.CustomerVo;
import com.ruoyi.mf.domain.bo.CustomerBo;
import com.ruoyi.common.orm.core.service.IBaseService;
import com.ruoyi.common.orm.core.page.TableDataInfo;

/**
 * 客户主表Service接口
 *
 * @author 数据小王子
 * 2023-12-06
 */
public interface ICustomerService extends IBaseService<Customer>
{
    /**
     * 查询客户主表
     *
     * @param customerId 客户主表主键
     * @return 客户主表
     */
    CustomerVo selectById(Long customerId);

    /**
     * 查询客户主表列表
     *
     * @param customerBo 客户主表Bo
     * @return 客户主表集合
     */
    List<CustomerVo> selectList(CustomerBo customerBo);

    /**
     * 分页查询客户主表列表
     *
     * @param customerBo 客户主表Bo
     * @return 分页客户主表集合
     */
    TableDataInfo<CustomerVo> selectPage(CustomerBo customerBo);

    /**
     * 新增客户主表
     *
     * @param customerBo 客户主表Bo
     * @return 结果:true 操作成功，false 操作失败
     */
    boolean insert(CustomerBo customerBo);

    /**
     * 修改客户主表
     *
     * @param customerBo 客户主表Bo
     * @return 结果:true 更新成功，false 更新失败
     */
    boolean update(CustomerBo customerBo);

    /**
     * 批量删除客户主表
     *
     * @param customerIds 需要删除的客户主表主键集合
     * @return 结果:true 删除成功，false 删除失败
     */
    boolean deleteByIds(Long[] customerIds);

}
