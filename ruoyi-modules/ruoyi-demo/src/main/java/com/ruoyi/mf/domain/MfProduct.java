package com.ruoyi.mf.domain;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.orm.core.domain.TreeEntity;

/**
 * 产品树对象 mf_product
 *
 * @author 数据小王子
 * 2023-11-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "mf_product")
public class MfProduct extends TreeEntity
{
    /** 产品id */
    @Id
    private Long productId;

    /** 产品名称 */
    private String productName;

    /** 产品状态（0正常 1停用） */
    private String status;

}
