package com.ruoyi.mf.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.mf.domain.Goods;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 客户主表对象 mf_customer
 *
 * @author 数据小王子
 * 2023-12-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "mf_customer")
public class Customer extends BaseEntity
{
    /** 客户id */
    @Id
    private Long customerId;

    /** 客户姓名 */
    private String customerName;

    /** 手机号码 */
    private String phonenumber;

    /** 客户性别 */
    private String gender;

    /** 客户生日 */
    private Date birthday;

    /** 客户描述 */
    private String remark;

    /** 商品子表信息 */
    private List<Goods> goodsList;

}
