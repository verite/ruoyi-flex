package com.ruoyi.mf.mapper;

import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.ruoyi.mf.domain.Goods;

/**
 * 商品子表Mapper接口
 *
 * @author 数据小王子
 * 2023-12-06
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods>
{

}
