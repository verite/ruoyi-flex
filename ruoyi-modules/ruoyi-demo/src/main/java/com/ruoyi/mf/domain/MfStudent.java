package com.ruoyi.mf.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 学生信息表对象 mf_student
 *
 * @author 数据小王子
 * 2023-11-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "mf_student")
public class MfStudent extends BaseEntity
{
    /** 编号 */
    @Id
    private Long studentId;

    /** 学生名称 */
    private String studentName;

    /** 年龄 */
    private Long studentAge;

    /** 爱好（0代码 1音乐 2电影） */
    private String studentHobby;

    /** 性别（1男 2女 3未知） */
    private String studentGender;

    /** 状态（0正常 1停用） */
    private String studentStatus;

    /** 生日 */
    private Date studentBirthday;

}
