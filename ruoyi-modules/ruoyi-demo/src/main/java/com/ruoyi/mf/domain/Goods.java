package com.ruoyi.mf.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serial;
import java.io.Serializable;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 商品子表对象 mf_goods
 *
 * @author 数据小王子
 * 2023-12-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "mf_goods")
public class Goods extends BaseEntity
{
    @Serial
    private static final long serialVersionUID = 1L;

    /** 商品id */
    @Id
    private Long goodsId;

    /** 客户id */
    private Long customerId;

    /** 商品名称 */
    private String name;

    /** 商品重量 */
    private Long weight;

    /** 商品价格 */
    private BigDecimal price;

    /** 商品时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    /** 商品种类 */
    private String type;


}
