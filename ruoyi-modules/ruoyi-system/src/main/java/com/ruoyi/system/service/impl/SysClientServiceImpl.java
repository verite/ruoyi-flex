package com.ruoyi.system.service.impl;


import org.springframework.stereotype.Service;
import com.ruoyi.system.service.ISysClientService;
import com.ruoyi.system.domain.SysClient;
import com.ruoyi.system.mapper.SysClientMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;

/**
 * 系统授权表 服务层实现。
 *
 *  @author dataprince数据小王子
 */
@Service
public class SysClientServiceImpl extends ServiceImpl<SysClientMapper, SysClient> implements ISysClientService {

}
