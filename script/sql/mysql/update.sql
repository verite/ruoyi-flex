--V4.1.8升级到V4.2.0：
-- sys_menu菜单表结构修改、去掉主键自增
ALTER TABLE `sys_menu`
	CHANGE COLUMN `menu_id` `menu_id` BIGINT(19) NOT NULL COMMENT '菜单ID' FIRST,
	CHANGE COLUMN `order_num` `order_num` INT(4) NULL DEFAULT '0' COMMENT '显示顺序' AFTER `parent_id`,
	CHANGE COLUMN `query` `query_param` VARCHAR(255) NULL DEFAULT NULL COMMENT '路由参数' COLLATE 'utf8mb4_bin' AFTER `component`,
	CHANGE COLUMN `is_frame` `is_frame` INT(1) NULL DEFAULT '1' COMMENT '是否为外链（0是 1否）' AFTER `query_param`,
	CHANGE COLUMN `is_cache` `is_cache` INT(1) NULL DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）' AFTER `is_frame`;
-- gen_table表去掉主键自增
ALTER TABLE `gen_table`
    CHANGE COLUMN `table_id` `table_id` BIGINT(19) NOT NULL COMMENT '编号' FIRST;
-- gen_table_column表去掉主键自增
ALTER TABLE `gen_table_column`
    CHANGE COLUMN `column_id` `column_id` BIGINT(19) NOT NULL COMMENT '编号' FIRST;

ALTER TABLE `sys_tenant` DROP COLUMN `create_dept`;

-- 修改数据库表的del_flag字段为smallint类型
ALTER TABLE `sys_client`
    CHANGE COLUMN `del_flag` `del_flag` SMALLINT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）' COLLATE 'utf8mb4_bin' AFTER `status`;
ALTER TABLE `sys_dept`
    CHANGE COLUMN `del_flag` `del_flag` SMALLINT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）' COLLATE 'utf8mb4_bin' AFTER `status`;
ALTER TABLE `sys_role`
    CHANGE COLUMN `del_flag` `del_flag` SMALLINT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）' COLLATE 'utf8mb4_bin' AFTER `status`;
ALTER TABLE `sys_tenant`
    CHANGE COLUMN `del_flag` `del_flag` SMALLINT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）' COLLATE 'utf8mb4_bin' AFTER `status`;
ALTER TABLE `sys_user`
    CHANGE COLUMN `del_flag` `del_flag` SMALLINT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）' COLLATE 'utf8mb4_bin' AFTER `status`;

----升级 from V4.2.0 to V5.0.0:
--修正“任务调度”的“任务管理”错误：
UPDATE `pj_job_info` SET `processor_info`='com.ruoyi.job.processors.StandaloneProcessorDemo' WHERE  `id`=1;
UPDATE `pj_job_info` SET `processor_info`='com.ruoyi.job.processors.BroadcastProcessorDemo' WHERE  `id`=2;
UPDATE `pj_job_info` SET `processor_info`='com.ruoyi.job.processors.MapProcessorDemo' WHERE  `id`=3;
UPDATE `pj_job_info` SET `processor_info`='com.ruoyi.job.processors.MapReduceProcessorDemo' WHERE  `id`=4;

--升级“文件管理配置”相关脚本
ALTER TABLE `sys_menu`
    CHANGE COLUMN `update_by` `update_by` BIGINT(19) NULL DEFAULT '0' COMMENT '更新者' AFTER `create_time`;
delete from sys_menu where menu_id in (1604, 1605);
insert into sys_menu values('1600', '文件查询', '118', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:query',        '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1601', '文件上传', '118', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:upload',       '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1602', '文件下载', '118', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:download',     '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1603', '文件删除', '118', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:remove',       '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1620', '配置列表', '118', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:list',   '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1621', '配置添加', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:add',    '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1622', '配置编辑', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:edit',   '#', 1, sysdate(), null, null, '');
insert into sys_menu values('1623', '配置删除', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:remove', '#', 1, sysdate(), null, null, '');
