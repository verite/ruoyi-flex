----升级 from V4.2.0 to V5.0.0:
--修正“任务调度”的“任务管理”错误：
UPDATE "public"."pj_job_info" SET "alarm_config"='{"alertThreshold":0,"silenceWindowLen":0,"statisticWindowLen":0}', "log_config"='{"type":1}', "processor_info"='com.ruoyi.job.processors.StandaloneProcessorDemo' WHERE  "id"=1;
UPDATE "public"."pj_job_info" SET "alarm_config"='{"alertThreshold":0,"silenceWindowLen":0,"statisticWindowLen":0}', "log_config"='{"type":1}', "processor_info"='com.ruoyi.job.processors.BroadcastProcessorDemo' WHERE  "id"=2;
UPDATE "public"."pj_job_info" SET "alarm_config"='{"alertThreshold":0,"silenceWindowLen":0,"statisticWindowLen":0}', "log_config"='{"type":1}', "processor_info"='com.ruoyi.job.processors.MapProcessorDemo' WHERE  "id"=3;
UPDATE "public"."pj_job_info" SET "alarm_config"='{"alertThreshold":0,"silenceWindowLen":0,"statisticWindowLen":0}', "log_config"='{"type":1}', "processor_info"='com.ruoyi.job.processors.MapReduceProcessorDemo' WHERE  "id"=4;

--修改powerjob的库结构的错误oid类型为text类型；
ALTER TABLE "pj_workflow_info"
ALTER COLUMN "pedag" TYPE TEXT,
	ALTER COLUMN "pedag" DROP NOT NULL,
	ALTER COLUMN "pedag" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_info"."pedag" IS '';
ALTER TABLE "pj_workflow_instance_info"
ALTER COLUMN "dag" TYPE TEXT,
	ALTER COLUMN "dag" DROP NOT NULL,
	ALTER COLUMN "dag" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_instance_info"."dag" IS '';
ALTER TABLE "pj_workflow_instance_info"
ALTER COLUMN "result" TYPE TEXT,
	ALTER COLUMN "result" DROP NOT NULL,
	ALTER COLUMN "result" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_instance_info"."result" IS '';
ALTER TABLE "pj_workflow_instance_info"
ALTER COLUMN "wf_context" TYPE TEXT,
	ALTER COLUMN "wf_context" DROP NOT NULL,
	ALTER COLUMN "wf_context" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_instance_info"."wf_context" IS '';
ALTER TABLE "pj_workflow_instance_info"
ALTER COLUMN "wf_init_params" TYPE TEXT,
	ALTER COLUMN "wf_init_params" DROP NOT NULL,
	ALTER COLUMN "wf_init_params" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_instance_info"."wf_init_params" IS '';
ALTER TABLE "pj_workflow_node_info"
ALTER COLUMN "extra" TYPE TEXT,
	ALTER COLUMN "extra" DROP NOT NULL,
	ALTER COLUMN "extra" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_node_info"."extra" IS '';
ALTER TABLE "pj_workflow_node_info"
ALTER COLUMN "node_params" TYPE TEXT,
	ALTER COLUMN "node_params" DROP NOT NULL,
	ALTER COLUMN "node_params" DROP DEFAULT;
COMMENT ON COLUMN "pj_workflow_node_info"."node_params" IS '';

--升级“文件管理配置”相关脚本
delete from sys_menu where menu_id in (1604, 1605);
insert into sys_menu values('1600', '文件查询', '118', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:query',        '#', 1, now(), null, null, '');
insert into sys_menu values('1601', '文件上传', '118', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:upload',       '#', 1, now(), null, null, '');
insert into sys_menu values('1602', '文件下载', '118', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:download',     '#', 1, now(), null, null, '');
insert into sys_menu values('1603', '文件删除', '118', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:oss:remove',       '#', 1, now(), null, null, '');
insert into sys_menu values('1620', '配置列表', '118', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:list',   '#', 1, now(), null, null, '');
insert into sys_menu values('1621', '配置添加', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:add',    '#', 1, now(), null, null, '');
insert into sys_menu values('1622', '配置编辑', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:edit',   '#', 1, now(), null, null, '');
insert into sys_menu values('1623', '配置删除', '118', '6', '#', '', '', '1', '0', 'F', '0', '0', 'system:ossConfig:remove', '#', 1, now(), null, null, '');

--postgresql创建于与mysql等效的的find_in_set函数
CREATE OR REPLACE FUNCTION find_in_set(
		value anyelement,
		string_list text)
		RETURNS integer
		LANGUAGE 'plpgsql'
		COST 100
		VOLATILE PARALLEL UNSAFE
	AS $BODY$
	DECLARE
position INTEGER;
BEGIN
		IF string_list = '' THEN
			RETURN 0;
ELSE
			position := array_position(string_to_array(string_list, ','), value::TEXT);
RETURN position;
END IF;
END;
$BODY$;

